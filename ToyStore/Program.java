package ToyStore;

import java.util.ArrayList;
import java.util.List;

public class Program {
    public static void main(String[] args) {

        Toy toy1 = new Toy(0, "Игрушка1", 25);
        Toy toy2 = new Toy(1, "Игрушка2", 40);
        Toy toy3 = new Toy(2, "Игрушка3", 3);

        List<Toy> toys = new ArrayList<Toy>();
        toys.add(toy3);
        toys.add(toy2);
        toys.add(toy1);

        Store Store = new Store(toys);
        Store.saveToyForLottery();
        Store.saveToyForLottery();
        Store.saveToyForLottery();
        Store.saveToyForLottery();
    }
}
