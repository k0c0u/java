import java.awt.*;
import java.util.ArrayList;

public class Category {

    private String name;
    private ArrayList<Product> Products;

    public Category() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Product> getProducts() {
        return Products;
    }

    public void setProducts(Product product) {
        Products.add(product);
    }
}