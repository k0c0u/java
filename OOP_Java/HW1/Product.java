
public class Product {

    private String name;
    private Integer price;
    private Integer rank;

    public Product() {

    }

    @Override
    public String toString() {
        return "Имя продукта: (" + name + "): " + ", цена: " + price + ", рейтинг: " + rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}