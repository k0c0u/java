package Java.HW3;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        UserMenu.printMenu();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Сделайте выбор: ");
        int num = scanner.nextInt();
        Game game = null;
        switch (num) {
            case 1:
                game = new NumberGame();
                break;
            case 2:
                game = new EnGame();
                break;
            case 3:
                game = new RuGame();
                break;
            default:
                System.out.println("Игры нет!");
        }
        System.out.println("Введите количество букв: ");
        int countWords = scanner.nextInt();
        System.out.println("Введите максимальное количество попыток: ");
        int maxAttempts = scanner.nextInt();
        scanner.nextLine();
        game.start(countWords, maxAttempts);

        while (game.getGameStatus().equals(GameStatus.START)) {

            System.out.println("ход: ");
            String answer = scanner.nextLine();
            game.play(answer);
        }
    }
}
