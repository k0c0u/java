package Java.HW3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Random;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractGame implements Game {

    Integer sizeWord;
    Integer attempts;
    String word;
    GameStatus gameStatus = GameStatus.INIT;

    @Override
    public void start(Integer sizeWord, Integer attempts) {
        this.sizeWord = sizeWord;
        this.attempts = attempts;
        word = generateWorld();
        this.gameStatus = GameStatus.START;

    }
    
    public void play(String value){
        if(attempts>1) {
            Answer answerGame = inputValue(value);
            if(answerGame.getBull() == sizeWord) {
                System.out.println("Победа!");
                gameStatus = GameStatus.WIN;
            }
            else {
                System.out.println("коров - " + answerGame.getCow() + " , быков - " + answerGame.getBull());
                attempts--;
                System.out.println("Осталось " + attempts + " попыток");
            }
        }
        else {
            System.out.println("Попытки исчерпаны" + word);
            gameStatus = GameStatus.LOSE;
        }

    }

    @Override
    public Answer inputValue(String value) {
        int bulls = 0;
        int cows = 0;
        for (int i = 0; i < value.length(); i++) {
            if (value.charAt(i) == word.charAt(i)) {
                bulls++;
            }
            Character character = value.charAt(i);
            if (word.contains(character.toString())) {
                cows++;
            }
        }
        return new Answer(value, cows, bulls);
    }

    @Override
    public GameStatus getGameStatus() {

        return gameStatus;
    }

    private String generateWorld() {
        Random random = new Random();
        List<String> charList = generateCharList();
        String resWorld = "";
        for (int i = 0; i < sizeWord; i++) {
            int randomIndex = random.nextInt(charList.size());
            resWorld = resWorld.concat(charList.get(randomIndex));
            charList.remove(randomIndex);
        }
        return resWorld;
    }

    abstract List<String> generateCharList();
}
