package Java.HW7;

import java.util.Scanner;
import java.util.logging.Logger;

public class ViewCalculator {

    private Calculator calculator;

    public ViewCalculator(Calculator calculator) {
        this.calculator = calculator;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        CalcLogger calcLog = new CalcLogger();
        calcLog.createLog("Программа запущена");
        while (true) {
            int argReal = printInt(scanner,"Введите вещественную часть комплексного числа: ");
            int argImaginary = printInt(scanner, "Введите мнимую часть комплексного числа: ");
            calcLog.createLog(argReal, argImaginary);
            while (true) {
                String playerInput = printString(scanner, "Введите команду (*, +, /, =) : ");
                if (playerInput.equals("+")) {
                    int argReal2 = printInt(scanner, "Введите вещественную часть второго комплексного числа: ");
                    int argImaginary2 = printInt(scanner, "Введите мнимую часть второго комплексного числа: ");
                    calculator.sum(argReal2, argImaginary2);
                    calcLog.createLog(playerInput);
                    calcLog.createLog(argReal2, argImaginary2);
                    continue;
                }
                if (playerInput.equals("*")) {
                    int argReal2 = printInt(scanner, "Введите вещественную часть второго комплексного числа: ");
                    int argImaginary2 = printInt(scanner, "Введите мнимую часть второго комплексного числа: ");
                    calculator.multi(argReal2, argImaginary2);
                    calcLog.createLog(playerInput);
                    calcLog.createLog(argReal2, argImaginary2);
                    continue;
                }
                if (playerInput.equals("/")) {
                    int argReal2 = printInt(scanner, "Введите вещественную часть второго комплексного числа: ");
                    int argImaginary2 = printInt(scanner, "Введите мнимую часть второго комплексного числа: ");
                    calculator.div(argReal2,argImaginary2);
                    calcLog.createLog(playerInput);
                    calcLog.createLog(argReal2, argImaginary2);
                    continue;
                }
                if (playerInput.equals("=")) {
                    String result = calculator.getResult();
                    System.out.println("Результат " + result);
                    calcLog.createLog(" = " + result);
                    break;
                }
            }
            String exitCalc = printString(scanner, "Выйти (Y/N)?");
            if (exitCalc.equalsIgnoreCase("Y")) {
                continue;
            }
            break;
        }
        scanner.close();
    }

    private String printString(Scanner scanner, String message) {
        System.out.print(message);
        return scanner.nextLine();
    }

    private int printInt(Scanner scanner, String message) {
        System.out.print(message);
        return Integer.parseInt(scanner.nextLine());
    }
}