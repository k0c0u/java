public interface Abilities {
    public String introduce();

    public boolean run(double distance);

    public boolean jump(double height);
}