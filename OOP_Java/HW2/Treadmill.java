public class Treadmill extends SportsEquipment{
    private  double distance;

    public Treadmill(double distance) {
        this.distance = distance;
    }

    @Override
    public double getInfo() {
        return distance;
    }
}
