public class HW2 {
    public static void main(String[] args) {
        SportsEquipment[] obstacleCourse = {
                new Wall(0.4),
                new Treadmill(30.0),
                new Wall(1.7),
                new Treadmill(190.0),
        };

        Abilities[] listOfAthletes = {
                new Human("People", 1.5, 150.0),
                new Cat("Tiger", 3.0, 200.0),
                new Robot("Transformer", 0.2, 700.0),
        };

        for (Abilities ability : listOfAthletes) {
            System.out.println("Полосу препятствий преодолел " + ability.introduce());
            for (SportsEquipment obstacle : obstacleCourse) {
                if (obstacle instanceof Wall) {
                    if (!ability.jump(obstacle.getInfo()))
                        break;
                } else {
                    if (!ability.run(obstacle.getInfo()))
                        break;
                }
            }
            System.out.println();
        }
    }
}