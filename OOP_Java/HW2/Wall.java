public class Wall extends SportsEquipment {
    double height;

    public Wall(double height) {
        this.height = height;
    }

    @Override
    public double getInfo() {
        return height;
    }
}
