package Java.HW5;

import java.util.Scanner;

public class ViewCalculator {

    private Calculator calculator;

    public ViewCalculator(Calculator calculator) {
        this.calculator = calculator;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int argReal = printInt(scanner,"Введите вещественную часть комплексного числа: ");
            int argImaginary = printInt(scanner, "Введите мнимую часть комплексного числа: ");
            while (true) {
                String playerInput = printString(scanner, "Введите команду (+, =) : ");
                if (playerInput.equals("+")) {
                    int argReal2 = printInt(scanner, "Введите вещественную часть второго комплексного числа: ");
                    int argImaginary2 = printInt(scanner, "Введите мнимую часть второго комплексного числа: ");
                    calculator.sum(argReal2,argImaginary2);
                    continue;
                }
                if (playerInput.equals("=")) {
                    String result = calculator.getResult();
                    System.out.println("Результат " + result);
                    break;
                }
            }
            String exitCalc = printString(scanner, "Выйти (Y/N)?");
            if (exitCalc.equalsIgnoreCase("Y")) {
                continue;
            }
            break;
        }
        scanner.close();
    }

    private String printString(Scanner scanner, String message) {
        System.out.print(message);
        return scanner.nextLine();
    }

    private int printInt(Scanner scanner, String message) {
        System.out.print(message);
        return Integer.parseInt(scanner.nextLine());
    }
}