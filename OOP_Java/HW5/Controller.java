package Java.HW5;


public class Controller {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        ViewCalculator view = new ViewCalculator(calculator);

        view.start();
    }
}
