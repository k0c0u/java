package Java.HW5;

public interface Calculable {
    Calculable sum(int argReal, int argImaginary);
    String getResult();
}
