package Java.HW5;

public final class Calculator implements Calculable {

    private double real;
    private double imaginary;

    public Calculator(int real, int imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }
    public Calculator() {
        this(0,0);

    }

    @Override
    public Calculable sum(int argReal, int argImaginary) {
        real += argReal;
        imaginary += argImaginary;
        return this;
    }

    @Override
    public String getResult() {

        return "" + real + " + " + imaginary + "i";
    }
}