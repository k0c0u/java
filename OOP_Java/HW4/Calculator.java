package Java.HW4;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
    public double sum(List<? extends Number> list) {
        double result = 0.0;
        for (Number n: list) {
            result += n.doubleValue();
        }
        return result;
    }

    public double multiply(List<? extends Number> list) {
        double result = 1.0;
        for (Number n: list) {
            result *= n.doubleValue();
        }
        return result;
    }

    public double division(List<? extends Number> list) {
        double result = list.get(0).doubleValue();
        for (int i = 1; i < list.size(); i++) {
            result = result / list.get(i).doubleValue();
        }
        return result;
    }
}
