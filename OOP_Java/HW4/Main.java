package Java.HW4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Calculator calc = new Calculator();
        List<Double> list1 = new ArrayList<>(Arrays.asList(10.0, 3.8, 7.1));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(3, 4, 5, 8));

        System.out.println(calc.sum(list1));
        System.out.println(calc.sum(list2));
        System.out.println(calc.multiply(list1));
        System.out.println(calc.multiply(list2));
        System.out.println(calc.division(list1));
        System.out.println(calc.division(list2));
    }
}